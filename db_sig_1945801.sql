-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2020 at 12:48 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sig`
--

-- --------------------------------------------------------

--
-- Table structure for table `jasaweb`
--

CREATE TABLE `jasaweb` (
  `id_perusahaan` int(8) NOT NULL,
  `nama_perusahaan` varchar(255) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `website` varchar(255) NOT NULL,
  `no_hp` varchar(25) NOT NULL,
  `alamat` text NOT NULL,
  `kota` varchar(50) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jasaweb`
--

INSERT INTO `jasaweb` (`id_perusahaan`, `nama_perusahaan`, `kategori`, `website`, `no_hp`, `alamat`, `kota`, `provinsi`, `latitude`, `longitude`) VALUES
(4, 'STMIK TIME', 'Swasta', 'stmik-time.ac.id', '+62614561932', 'Jalan Merbabu No.32 aa-bb, Pusat Ps., Kec. Medan Kota, Kota Medan, Sumatera Utara 20212', 'Medan', 'Sumatera Utara', '3.586434', '98.689480'),
(5, 'STMIK - STIE Mikroskil', 'Swasta', 'mikroskil.ac.id', '+62614573767', 'Jl. M.H Thamrin No.140 Kel, Pusat Ps., Kec. Medan Kota, Kota Medan, Sumatera Utara 20212', 'Medan', 'Sumatera Utara', '3.587491', '98.690728'),
(6, 'STIE Eka Prasetya', 'Swasta', 'eka-prasetya.ac.id', '+62614571198', 'Jl. Merapi No.8, Pusat Ps., Kec. Medan Kota, Kota Medan, Sumatera Utara 20212', 'Medan', 'Sumatera Utara', '3.586015', '98.686764'),
(7, 'Universitas Sumatera Utara', 'Negeri', 'usu.ac.id', '', 'Jl. Dr. Mansyur, Padang Bulan, Kec. Medan Baru, Kota Medan, Sumatera Utara 20153', 'Medan', 'Sumatera Utara', '3.567906', '98.657671'),
(8, 'STIE Indonesia', 'Swasta', 'stindomedan.ac.id', '+62617321118', 'Jalan Perbaungan No.2, Sei Rengas II, Kec. Medan Area, Kota Medan, Sumatera Utara 20214', 'Medan', 'Sumatera Utara', '3.587042', '98.694629'),
(9, 'Institut Bisnis IT&B', 'Swasta', 'itnb.ac.id', '+62614530505', 'Jl. Mahoni No.16, Gaharu, Kec. Medan Tim., Kota Medan, Sumatera Utara 20235', 'Medan', 'Sumatera Utara', '3.598684', '98.680622'),
(10, 'Institut Teknologi Medan', 'Swasta', 'itm.ac.id', '+62617363771', 'Jl. Gedung Arca No.52, Teladan Bar., Kec. Medan Kota, Kota Medan, Sumatera Utara 20217', 'Medan', 'Sumatera Utara', '3.567321', '98.695767'),
(11, 'Universitas HKBP Nommensen Medan', 'Swasta', 'uhn.ac.id', '+62614522922', 'Jl. Sutomo No.4A, Perintis, Kec. Medan Tim., Kota Medan, Sumatera Utara 20235', 'Medan', 'Sumatera Utara', '3.596487', '98.681256'),
(12, 'UNIVERSITAS NEGERI MEDAN', 'Negeri', 'unimed.ac.id', '+62616613365', 'Jl. Rumah Sakit H., Kenangan Baru, Kec. Percut Sei Tuan, Kabupaten Deli Serdang, Sumatera Utara 20371', 'Medan', 'Sumatera Utara', '3.606666', '98.716002'),
(13, 'Universitas Medan Area', 'Swasta', 'uma.ac.id', '+62618201994', 'Jl. Setia Budi No.79 B, Tj. Rejo, Kec. Medan Sunggal, Kota Medan, Sumatera Utara 20112', 'Medan', 'Sumatera Utara', '3.576728', '98.643784'),
(14, 'Politeknik Negeri Medan', 'Negeri', 'polmed.ac.id', '+62618210436', 'Universitas Sumatera Utara Kampus USU, Jl. Almamater No.1, Padang Bulan, Kota Medan, Sumatera Utara 20155', 'Medan', 'Sumatera Utara', '3.564160', '98.654604'),
(15, 'Universitas Methodist Indonesia', 'Swasta', 'methodist.ac.id', '+62614157882', 'Jl. Hang Tuah No.8, Madras Hulu, Kec. Medan Polonia, Kota Medan, Sumatera Utara 20151', 'Medan', 'Sumatera Utara', '3.578705', '98.670787'),
(16, 'Universitas Islam Sumatera Utara', 'Swasta', 'uisu.ac.id', '+62617869790', 'Jl. Sisingamangaraja No.Kelurahan, Teladan Bar., Kec. Medan Kota, Kota Medan, Sumatera Utara 20217', 'Medan', 'Sumatera Utara', '3.562355', '98.693862'),
(17, 'Universitas Katolik Santo Thomas Medan', 'Swasta', 'ust.ac.id', '', 'Jl. Setia Budi, Kp. Tengah, Kec. Medan Tuntungan, Kota Medan, Sumatera Utara 20135', 'Medan', 'Sumatera Utara', '3.544061', '98.621543'),
(18, 'Universitas Darma Agung', 'Swasta', 'darmaagung.ac.id', '+62614535631', 'Jl. DR. TD Pardede No.21, Petisah Hulu, Kec. Medan Baru, Kota Medan, Sumatera Utara 20153', 'Medan', 'Sumatera Utara', '3.578781', '98.664800'),
(19, 'STIE - STMIK IBBI', 'Swasta', 'ibbi.ac.id', '+62614567111', 'Jl. Sei Deli No.18, Silalas, Kec. Medan Bar., Kota Medan, Sumatera Utara 20236', 'Medan', 'Sumatera Utara', '3.592287', '98.672427'),
(20, 'AMIK MBP MEDAN', 'Swasta', 'amikmbp.ac.id', '+6281214793858', 'politeknik mbp medan, Padang Bulan, Kec. Medan Baru, Kota Medan, Sumatera Utara 20222', 'Medan', 'Sumatera Utara', '3.559679', '98.662053');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jasaweb`
--
ALTER TABLE `jasaweb`
  ADD PRIMARY KEY (`id_perusahaan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jasaweb`
--
ALTER TABLE `jasaweb`
  MODIFY `id_perusahaan` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
